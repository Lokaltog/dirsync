import asyncio
import binascii
import functools
import os
import umsgpack


class DirSyncServerProtocol(asyncio.Protocol):
	def __init__(self, logger, loop, directory, secret):
		self.logger = logger
		self.loop = loop
		self.directory = directory
		self.secret = secret

		self.transport = None
		self.authenticated = False
		self.file_data = {}
		self.file_lock = asyncio.Lock()

		self._command_handlers = {
			'_err': self._handle_err,
			'auth': self._handle_command_auth,
			'file': self._handle_command_file,
			'chunk': self._handle_command_chunk,
			'del': self._handle_command_del,
		}

	def connection_made(self, transport):
		self.logger.debug('Connection received from {}:{}'.format(*transport.get_extra_info('peername')))
		self.transport = transport

	def data_received(self, data):
		try:
			command, data = umsgpack.unpackb(data)
		except TypeError:
			self.logger.critical('Invalid msgpacked data received!')

		if command not in self._command_handlers:
			self.logger.error('Ignoring invalid client command "{}"'.format(command))
			return

		self.loop.create_task(self._command_handlers[command](*data))

	def send_data(self, command, *data):
		self.transport.write(umsgpack.packb((command, data)))

	def connection_lost(self, data):
		self.logger.debug('Connection lost')

	def auth_required(func):
		@functools.wraps(func)
		def wrapper(self, *args, **kwargs):
			if not self.authenticated:
				self.send_err('Not authenticated')
				self.transport.close()
			return func(self, *args, **kwargs)
		return wrapper

	def send_err(self, message):
		self.send_data('_err', message)

	async def _handle_err(self, message):
		self.logger.error('Client error: {}'.format(message))

	async def _handle_command_auth(self, secret):
		if secret != self.secret:
			self.send_err('Invalid secret')
			self.transport.close()
			return

		self.logger.debug('Client authenticated')
		self.authenticated = True
		self.send_data('auth', True)

	@auth_required
	async def _handle_command_file(self, path, mtime, size, total_chunks):
		local_path = os.path.join(self.directory, path)
		self.logger.debug('Received info for file "{}" (mtime {} size {})'.format(path, mtime, size))

		if path in self.file_data:
			# File is already being written to, block until it can be written to again
			await self.file_data[path]['lock'].acquire()

		self.file_data[path] = dict(
			mtime=mtime,
			size=size,
			fp=None,
			local_path=local_path,
			total_chunks=total_chunks,
			written_chunks=0,
			checksum=0,
			lock=asyncio.Lock(),
		)

		await self.file_data[path]['lock'].acquire()

		req_offset = 0
		try:
			st = os.stat(local_path)
			if st.st_mtime == mtime and st.st_size == size:
				# Modification time and size are equal, assuming identical file
				self.logger.debug('{}: No modification detected'.format(local_path))
				self.file_data[path]['fp'] = open(local_path, 'wb')
				return
			if st.st_mtime == mtime and st.st_size != size:
				# Modification time is equal but size differs, assuming partial transfer
				self.logger.debug('{}: Partial transfer'.format(local_path))
				req_offset = st.st_size
			self.file_data[path]['fp'] = open(local_path, 'wb')
		except FileNotFoundError:
			try:
				self.logger.debug('{}: Does not exist'.format(local_path))
				parent_path = os.path.dirname(local_path)
				os.makedirs(parent_path, 0o755, exist_ok=True)
				self.file_data[path]['fp'] = open(local_path, 'xb')
			except FileExistsError:
				# We've already opened this file for writing
				assert self.file_data[path]['fp']
			except OSError:
				self.logger.error('Cannot open "{}" for writing'.format(local_path))
				self.send_err('Error while opening file "{}"'.format(path))
				return

		self.send_data('req', path, req_offset)

	@auth_required
	async def _handle_command_chunk(self, path, offset, chunk, checksum):
		if self.file_data[path]['written_chunks'] == self.file_data[path]['total_chunks']:
			self.logger.debug('Finished writing file "{}"'.format(path))
			# Last chunk received, flush write buffer and update file mtime
			self.file_data[path]['fp'].flush()
			os.utime(self.file_data[path]['local_path'],
					times=(self.file_data[path]['mtime'], self.file_data[path]['mtime']))

			if checksum != self.file_data[path]['checksum']:
				# Checksums differ, so the file either has become corrupted or changed on the client during transmission
				# Remove file and request entire file from client
				self.file_data[path]['fp'].close()
				self.file_data[path]['lock'].release()
				os.unlink(self.file_data[path]['local_path'])
				self.send_data('file', path)
				del self.file_data[path]
				return

			# Cleanup cached file data
			self.file_data[path]['fp'].close()
			self.file_data[path]['lock'].release()
			del self.file_data[path]

			return

		# Write received chunk to file
		self.file_data[path]['fp'].seek(offset, os.SEEK_SET)
		self.file_data[path]['fp'].write(chunk)
		self.file_data[path]['written_chunks'] += 1
		self.file_data[path]['checksum'] = binascii.crc32(chunk, self.file_data[path]['checksum'])

	@auth_required
	async def _handle_command_del(self, path):
		local_path = os.path.join(self.directory, path)
		self.logger.debug('Deleting file "{}"'.format(local_path))
		try:
			self.file_data[path]['fp'].close()
			self.file_data[path]['lock'].release()
			del self.file_data[path]
		except KeyError:
			pass
		os.unlink(local_path)
