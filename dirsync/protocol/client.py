import asyncio
import binascii
import math
import os
import umsgpack
import pyinotify


class DirSyncClientProtocol(asyncio.Protocol):
	CHUNK_SIZE = 0x1000

	def __init__(self, logger, loop, directory, secret):
		self.logger = logger
		self.loop = loop
		self.directory = directory
		self.secret = secret

		self.transport = None
		self.file_queue = asyncio.Queue()
		self.authenticated = asyncio.Event()

		self._command_handlers = {
			'_err': self._handle_err,
			'req': self._handle_command_req,
			'file': self._handle_command_file,
			'auth': self._handle_command_auth,
		}

		# Setup recursive inotify watch
		# Works great for smaller directories, but performance can suffer on large directories with many files
		wm = pyinotify.WatchManager()
		mask = pyinotify.IN_CREATE | pyinotify.IN_DELETE | pyinotify.IN_CLOSE_WRITE
		self.notifier = pyinotify.AsyncioNotifier(wm, self.loop, default_proc_fun=self._handle_notify)
		wm.add_watch(self.directory, mask, rec=True, auto_add=True)

	def __del__(self):
		self.notifier.stop()

	def connection_made(self, transport):
		self.logger.debug('Connected to {}:{}'.format(*transport.get_extra_info('peername')))
		self.transport = transport

		self.loop.create_task(self._handle_file_queue())

		self.send_auth()
		self.loop.create_task(self.send_file_list())

	def data_received(self, data):
		try:
			command, data = umsgpack.unpackb(data)
		except TypeError:
			self.logger.critical('Invalid msgpacked data received!')

		if command not in self._command_handlers:
			self.logger.error('Ignoring invalid client command "{}"'.format(command))
			return

		self.loop.create_task(self._command_handlers[command](*data))

	def send_data(self, command, *data):
		self.transport.write(umsgpack.packb((command, data)))

	def connection_lost(self, data):
		self.logger.debug('Connection lost, aborting...')
		self.loop.stop()

	def send_auth(self):
		self.send_data('auth', self.secret)

	async def send_file_list(self):
		'''Add all files to file queue.
		'''
		await self.authenticated.wait()

		def scan_tree(directory):
			for entry in os.scandir(directory):
				if entry.is_dir(follow_symlinks=False):
					yield from scan_tree(entry.path)
				else:
					yield entry

		for entry in scan_tree(self.directory):
			if not entry.name.startswith('.') and not entry.is_dir():
				st = entry.stat()
				self.file_queue.put_nowait((
					os.path.relpath(entry.path, self.directory),
					st.st_mtime,
					st.st_size,
					math.ceil(st.st_size / self.CHUNK_SIZE)))

	async def send_file(self, path):
		'''Add single file to file queue.
		'''
		await self.authenticated.wait()

		local_path = os.path.join(self.directory, path)
		st = os.stat(local_path)
		self.file_queue.put_nowait((
			os.path.relpath(local_path, self.directory),
			st.st_mtime,
			st.st_size,
			math.ceil(st.st_size / self.CHUNK_SIZE)))

	def _handle_notify(self, event):
		'''Handle inotify events.

		Adds tuple with information about changed files to the file queue.

		FIXME Detect file changes during transmission, this could be accomplished by
		e.g. checking a flag for the file in transit in read_chunks() in
		_handle_command_req()

		FIXME Store the changed files in a set(), and poll for changes with an
		interval to avoid re-sending the file if it's changed multiple times during a
		short interval
		'''
		if event.dir:
			return

		if event.mask & pyinotify.IN_DELETE:
			self.logger.debug('File "{}" was deleted'.format(event.pathname))
			self.send_data('del', os.path.relpath(event.pathname, self.directory))
			return

		self.logger.debug('File "{}" was changed'.format(event.pathname))

		try:
			self.loop.create_task(self.send_file(event.pathname))
		except FileNotFoundError:
			# Weird, the file must have been deleted between the inotify trigger and this code
			pass

	async def _handle_err(self, message):
		self.logger.error('Server error: {}'.format(message))

	async def _handle_command_auth(self, status):
		if status:
			self.logger.debug('Client authenticated')
			self.authenticated.set()

	async def _handle_command_req(self, path, offset):
		self.logger.debug('Server requested file "{}" offset {}'.format(path, offset))
		local_path = os.path.join(self.directory, path)

		def read_chunks(fp, chunk_size=self.CHUNK_SIZE):
			offset = 0
			checksum = 0
			while True:
				chunk = fp.read(chunk_size)
				checksum = binascii.crc32(chunk, checksum)
				yield offset, chunk, checksum
				offset += len(chunk)
				if not len(chunk):
					break

		self.logger.debug('Transmitting file "{}"...'.format(path))
		with open(local_path, 'rb') as fp:
			fp.seek(offset, os.SEEK_SET)
			for offset, chunk, checksum in read_chunks(fp):
				self.send_data('chunk', path, offset, chunk, checksum)

	async def _handle_command_file(self, path):
		'''Send updated file info to server.
		'''
		await self.send_file(path)

	async def _handle_file_queue(self):
		'''File queue handler task.

		Monitors the file queue and sends changed files to server. Expects queue
		items to be a tuple consisting of the relative file path, modification time,
		size in bytes and total chunks.
		'''
		try:
			while True:
				try:
					item = await asyncio.wait_for(self.file_queue.get(), 5)
				except asyncio.TimeoutError:
					continue
				except asyncio.CancelledError:
					self.logger.debug('Queue handler task cancelled')
					break

				self.logger.debug('Sending info for file "{}" (mtime {} size {})'.format(*item))
				self.send_data('file', *item)
		finally:
			self.transport.close()
