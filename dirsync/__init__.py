import random


def random_secret(length=16):
	'''Generate easy to remember and pronounce random secrets by alternating wovels and consonants.
	'''
	rand = random.SystemRandom()
	return ''.join([
		rand.choice('aeiouy') if i % 2 else
		rand.choice('bcdfghjklmnpqrstvwxz')
		for i in range(length)])
