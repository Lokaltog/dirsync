# Directory sync client and server

This script is built with extensibility in mind, e.g. with support for two-way
syncing. The server supports multiple clients, but currently doesn't handle locking
or conflict resolution if the clients have different files with the same name.

This server/client implements a protocol using asyncio from the Python standard
library and msgpack. A more portable alternative could be implemented using
e.g. Twisted + [AMP](http://amp-protocol.net/) instead.

## Installation and usage

The scripts require Python 3.5.

```sh
pyvenv venv
. venv/bin/activate.fish
./setup.py install

dirsync-server --debug --secret=mysecret --ssl-cert-chain certs/dummy.pem dir1
dirsync-client --ssl-cafile=certs/dummy.crt --debug localhost mysecret dir2
```

## Known issues

* The script assumes that both the server and client are trusted, and currently
  doesn't perform any checks like verifying the location of the target files. An
  attacker will easily be able to delete any file the user has access to by
  e.g. sending the `del` command with a relative path outside the shared directory
  (e.g. `../../../../some/file`), this can easily be fixed if required.
* The auth method should be rate limited to prevent brute forcing the shared secret.
* The current file comparison algorithm can be improved a lot. With some extra code
  it should be trivial to implement a way of only sending the chunks that are
  different on the client and server. This would save network bandwidth, but also
  introduce a performance penalty (particularily for large files) as checksums or the
  offset for the first different byte would need to be calculated at both the client
  side and server side.
* The script doesn't properly handle many corner cases, it may break if e.g. the
  client is transmitting a file, and the file is modified multiple times during data
  transmission - the client will send chunks from both the original file and the
  modified file in random order. This should be caught by the checksum check in the
  last chunks though (where the server will assume the entire file has changed and
  request it again from the client), but this may cause unneccessary resource usage.
* The script doesn't sync (create or delete) empty directories.
* File IO is blocking the event loop, but it could possibly be moved into a
  `ThreadedPoolExecutor` or something like that although this would require extra
  code to handle stuff like write locks, and I feel it's beyond the scope of this
  exercise.
* The script hasn't been written with resource usage or performance in mind. msgpack
  is more inefficient than implementing a custom protocol, but it works fine as a
  proof-of-concept.
