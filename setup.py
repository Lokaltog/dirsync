#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
	name='DirSync',
	version='0.0.1',
	description='Directory synchronizer programming exercise.',
	author='Kim Silkebækken',
	author_email='kim@silkebaekken.no',
	url='https://bitbucket.org/Lokaltog/dirsync/',
	install_requires=[
		'pyinotify==0.9.6',
		'u-msgpack-python==2.1',
	],
	packages=find_packages(),
	scripts=[
		'bin/dirsync-server',
		'bin/dirsync-client',
	],
)
